package redis

import (
	"balanceplatform-api-action/logger"
	"balanceplatform-api-action/utils"
	"context"
	"github.com/go-redis/redis/v8"
	"strings"
)

// structure for actions
type Action struct {
	name   string
	config string
}

var ctx = context.Background()

// ActionRegistration - registrtion all actions in redis
func ActionRegistration() {

	// list of actions for registration
	var actions = []Action{
		{
			name: utils.Cfg.IfnsCheckEventName,
			config: `{
			  "title":"IFNS Check",
			  "name":"` + utils.Cfg.IfnsCheckEventName + `",
			  "class":"stateless-action",
			  "inputs":1,
			  "outputs":0,
			  "data":{
				"isCustom":true,
				"returnable":false
			  },
              "paramsDescription":[
		  	]
			}`,
		},
		{
			name: utils.Cfg.IfnsInnEventName,
			config: `{
		  "title":"IFNS Inn",
		  "name":"` + utils.Cfg.IfnsInnEventName + `",
		  "class":"stateless-action",
		  "inputs":1,
		  "outputs":0,
		  "data":{
			"isCustom":true,
			"returnable":false,
			"name":"",
			"surname":"",
			"middlename":"",
			"birthdate":"",
			"doctype":"",
			"docid":""
		  },
		  "paramsDescription":[
			{
			  "name":"name",
			  "value":"Имя"
			},
			{
			  "name":"surname",
			  "value":"Фамилия"
			},
			{
			  "name":"middlename",
			  "value":"Отчество"
			},
			{
			  "name":"birthdate",
			  "value":"Дата рождения"
			},
			{
			  "name":"doctype",
			  "value":"Тип документа"
			},
			{
			  "name":"docid",
			  "value":"Серия/Номер"
			}
		  ]
		}`,
		},
		{
			name: utils.Cfg.SparkStatusEventName,
			config: `{
		  "title":"SPARK Status",
		  "name":"` + utils.Cfg.SparkStatusEventName + `",
		  "class":"stateless-action",
		  "inputs":1,
		  "outputs":0,
		  "data":{
			"isCustom":true,
			"returnable":false
		  },
		  "paramsDescription":[
		  ]
		}`,
		},
		{
			name: utils.Cfg.SparkBnkEventName,
			config: `{
		  "title":"SPARK bankruptcy",
		  "name":"` + utils.Cfg.SparkBnkEventName + `",
		  "class":"stateless-action",
		  "inputs":1,
		  "outputs":0,
		  "data":{
			"isCustom":true,
			"returnable":false
		  },
		  "paramsDescription":[
		  ]
		}`,
		},
	}

	//logger.Log.Warn("Redis current actions: ", actions)

	var client *redis.Client

	logger.Log.Warn("Redis - Connection string: ", utils.Cfg.RedisConnection)
	//var index = strings.Index(utils.Cfg.RedisConnection, "")
	var index = strings.Index(utils.Cfg.RedisConnection, "redis://")
	if index == 0 {
		utils.Cfg.RedisConnection = utils.Cfg.RedisConnection[8:]
	}

	indexName := strings.Index(utils.Cfg.RedisConnection, ":")
	indexPass := strings.Index(utils.Cfg.RedisConnection, "@")
	var userName string
	var password string
	var address string
	if index >= 0 {
		userName = utils.Cfg.RedisConnection[:indexName]
		password = utils.Cfg.RedisConnection[indexName+1 : indexPass]
		address = utils.Cfg.RedisConnection[indexPass+1:]

		if len(userName) > 0 {
			logger.Log.Warn("Redis with username - userName: ", userName)
			client = redis.NewClient(&redis.Options{
				Username: userName,
				Addr:     address,
				Password: password,
				DB:       0,
			})
		} else {
			logger.Log.Warn("Redis without username")
			client = redis.NewClient(&redis.Options{
				Addr:     address,
				Password: password,
				DB:       0,
			})
		}
		logger.Log.Warn("Redis - password: ", password)
		logger.Log.Warn("Redis - address: ", address)

	} else {
		address = utils.Cfg.RedisConnection
		logger.Log.Warn("Redis without username - address: ", address)
		logger.Log.Warn("Redis - password: ", password)

		client = redis.NewClient(&redis.Options{
			Addr:     address,
			Password: password,
			DB:       0,
		})
	}

	logger.Log.Warn("Redis - client: ", client)

	//	all actions in range
	for _, action := range actions {
		result, err := client.HSet(ctx, utils.Cfg.RedisKeyName, action.name, action.config).Result()
		logger.Log.Warn("Result of HSet ", action.name, result)
		// handle the error
		if err != nil {
			logger.Log.Warn("Error throw adding action to Redis ", action.name, err)
		} else {
			logger.Log.Warn("Success throw adding action to Redis ", action.name, utils.Cfg.RedisKeyName)
			result, _ := client.HGet(ctx, utils.Cfg.RedisKeyName, action.name).Result()
			logger.Log.Warn("Try to read value", action.name, result)

		}

	}

}
