package rabbit

import (
	"balanceplatform-api-action/http"
	"balanceplatform-api-action/logger"
	"balanceplatform-api-action/types"
	"balanceplatform-api-action/utils"
	"encoding/json"
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	log "github.com/sirupsen/logrus"
	//"github.com/streadway/amqp"
	"os"
)

func handleError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

//func ConsumeV5(q string, ch chan []byte, bite bool) {
//	logger.Log.Info("Start Consumer V3")
//
//	conn, err := amqp.Dial(utils.Cfg.BrokerConnection)
//	if err != nil {
//		logger.Log.Error("Can't connect to AMQP")
//	} else {
//		logger.Log.Info("Connected to AMQP (consume) for ", q)
//	}
//	defer conn.Close()
//
//	stopChan := make(chan bool)
//
//	amqpChannel, err := conn.Channel()
//	if err != nil {
//		logger.Log.Error("Can't create a amqpChannel: ")
//	} else {
//		logger.Log.Info("amqpChannel created successful (consume) for ", q)
//	}
//	defer amqpChannel.Close()
//
//	logger.Log.Info("Creating queue ", q)
//
//	queue, err := amqpChannel.QueueDeclare(
//		q,
//		true,
//		false,
//		false,
//		false,
//		//nil,
//		amqp.Table{"x-queue-mode": "lazy"},
//	)
//	if err != nil {
//		logger.Log.Error("Could not declare queue: ", err)
//	} else {
//		logger.Log.Info("Queue declared successful - ", q)
//	}
//
//	// Create a binding between queue and exchanges to rabbitmq
//	err = amqpChannel.QueueBind(q, "", "ex-custom-actions", false, nil)
//	//err := channelRabbitMQ.QueueBind(queue, "", "award", false, nil)
//	if err != nil {
//		fmt.Println(err)
//	} else {
//		logger.Log.Warn(q, " has bind")
//	}
//
//	//err = amqpChannel.Qos(1, 0, false)
//	//handleError(err, "Could not configure QoS")
//
//	messageChannel, err := amqpChannel.Consume(
//		queue.Name,
//		"",
//		true,
//		false,
//		false,
//		false,
//		nil,
//	)
//	if err != nil {
//		logger.Log.Error("Could not register consumer: ")
//	} else {
//		logger.Log.Info("Consumer registered successful: ", queue.Name)
//	}
//
//	if bite {
//
//		go func() {
//			log.Printf("Consumer ready, PID: %d", os.Getpid())
//			for d := range messageChannel {
//				log.Printf("Received a message: %s", d.Body)
//				//msg :=
//				//if err := d.Ack(false); err != nil {
//				//	log.Printf("Error acknowledging message : %s", err)
//				//} else {
//				//	log.Printf("Acknowledged message")
//				//}
//
//				var payload types.RequestFromRabbit
//
//				//logger.Log.Warn("before marshal", string(d.Body))
//				//logger.Log.Warn("after marshal", temp)
//				err := json.Unmarshal(d.Body, &payload)
//				convertedBite, err := strconv.ParseInt(payload.Id, 10, 64)
//				if err != nil {
//					log.Error("Error: ", err)
//				} else {
//
//					log.Infof("after marshal | ID: %s Trigger: %s aName: %s bite: %t", payload.Id, payload.Event, payload.Action.Name, convertedBite%2 != 0)
//				}
//
//				switch {
//				case q == utils.Cfg.IfnsCheckQueue && payload.Action.Name == utils.Cfg.IfnsCheckEventName && convertedBite%2 != 0:
//					_ = messageHandleWithoutQ(payload, ch)
//					break
//				case q == utils.Cfg.IfnsInnQueue && payload.Action.Name == utils.Cfg.IfnsInnEventName:
//					_ = messageHandleWithoutQ(payload, ch)
//					break
//				case q == utils.Cfg.SparkStatusQueue && payload.Action.Name == utils.Cfg.SparkStatusEventName && convertedBite%2 != 0:
//					_ = messageHandleWithoutQ(payload, ch)
//					break
//				case q == utils.Cfg.SparkBnkQueue && payload.Action.Name == utils.Cfg.SparkBnkEventName && convertedBite%2 != 0:
//					_ = messageHandleWithoutQ(payload, ch)
//					break
//				}
//
//			}
//		}()
//	} else {
//
//		go func() {
//			log.Printf("Consumer ready, PID: %d", os.Getpid())
//			for d := range messageChannel {
//				log.Printf("Received a message: %s", d.Body)
//				//msg :=
//				//if err := d.Ack(false); err != nil {
//				//	log.Printf("Error acknowledging message : %s", err)
//				//} else {
//				//	log.Printf("Acknowledged message")
//				//}
//
//				var payload types.RequestFromRabbit
//
//				//logger.Log.Warn("before marshal", string(d.Body))
//				//logger.Log.Warn("after marshal", temp)
//				err := json.Unmarshal(d.Body, &payload)
//				convertedBite, err := strconv.ParseInt(payload.Id, 10, 64)
//				if err != nil {
//					log.Error("Error: ", err)
//				} else {
//
//					log.Infof("after marshal | ID: %s Trigger: %s aName: %s bite: %t", payload.Id, payload.Event, payload.Action.Name, convertedBite%2 == 0)
//				}
//
//				switch {
//				case q == utils.Cfg.IfnsCheckQueue && payload.Action.Name == utils.Cfg.IfnsCheckEventName && convertedBite%2 == 0:
//					_ = messageHandleWithoutQ(payload, ch)
//					break
//				case q == utils.Cfg.SparkStatusQueue && payload.Action.Name == utils.Cfg.SparkStatusEventName && convertedBite%2 == 0:
//					_ = messageHandleWithoutQ(payload, ch)
//					break
//				case q == utils.Cfg.SparkBnkQueue && payload.Action.Name == utils.Cfg.SparkBnkEventName && convertedBite%2 == 0:
//					_ = messageHandleWithoutQ(payload, ch)
//					break
//				}
//
//			}
//		}()
//	}
//
//	// Остановка для завершения программы
//	<-stopChan
//
//}

func ConsumeV4(q string, ch chan []byte) {
	logger.Log.Info("Start Consumer V4")

	conn, err := amqp.Dial(utils.Cfg.BrokerConnection)
	if err != nil {
		logger.Log.Error("Can't connect to AMQP")
	} else {
		logger.Log.Info("Connected to AMQP (consume) for ", q)
	}
	defer conn.Close()

	stopChan := make(chan bool)

	amqpChannel, err := conn.Channel()
	if err != nil {
		logger.Log.Error("Can't create a amqpChannel: ")
	} else {
		logger.Log.Info("amqpChannel created successful (consume) for ", q)
	}
	defer amqpChannel.Close()

	logger.Log.Info("Creating queue ", q)

	queue, err := amqpChannel.QueueDeclare(
		q,
		true,
		false,
		false,
		false,
		//nil,
		amqp.Table{"x-queue-mode": "lazy"},
	)
	if err != nil {
		logger.Log.Error("Could not declare queue: ", err)
	} else {
		logger.Log.Info("Queue declared successful - ", q)
	}

	// Create a binding between queue and exchanges to rabbitmq
	err = amqpChannel.QueueBind(q, "", "ex-custom-actions", false, nil)
	//err := channelRabbitMQ.QueueBind(queue, "", "award", false, nil)
	if err != nil {
		fmt.Println(err)
	} else {
		logger.Log.Warn(q, " has bind")
	}

	//err = amqpChannel.Qos(1, 0, false)
	//handleError(err, "Could not configure QoS")

	messageChannel, err := amqpChannel.Consume(
		queue.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		logger.Log.Error("Could not register consumer: ")
	} else {
		logger.Log.Info("Consumer registered successful: ", queue.Name)
	}

	go func() {
		log.Printf("Consumer ready, PID: %d", os.Getpid())
		for d := range messageChannel {
			log.Printf("Received a message: %s", d.Body)
			//msg :=
			//if err := d.Ack(false); err != nil {
			//	log.Printf("Error acknowledging message : %s", err)
			//} else {
			//	log.Printf("Acknowledged message")
			//}

			var payload types.RequestFromRabbit

			//logger.Log.Warn("before marshal", string(d.Body))
			//logger.Log.Warn("after marshal", temp)
			err := json.Unmarshal(d.Body, &payload)
			if err != nil {
				log.Error("Error: ", err)
			} else {

				log.Infof("after marshal | ID: %s Trigger: %s aName: %s", payload.Id, payload.Event, payload.Action.Name)
			}

			switch {
			case q == utils.Cfg.IfnsCheckQueue && payload.Action.Name == utils.Cfg.IfnsCheckEventName:
				_ = messageHandleWithoutQ(payload, ch)
				break
			case q == utils.Cfg.IfnsInnQueue && payload.Action.Name == utils.Cfg.IfnsInnEventName:
				_ = messageHandleWithoutQ(payload, ch)
				break
			case q == utils.Cfg.SparkStatusQueue && payload.Action.Name == utils.Cfg.SparkStatusEventName:
				_ = messageHandleWithoutQ(payload, ch)
				break
			case q == utils.Cfg.SparkBnkQueue && payload.Action.Name == utils.Cfg.SparkBnkEventName:
				_ = messageHandleWithoutQ(payload, ch)
				break
			}

		}
	}()

	// Остановка для завершения программы
	<-stopChan

}

//func ConsumeV3(q string, ch chan []byte) {
//	logger.Log.Info("Start Consumer V3")
//
//	conn, err := amqp.Dial(utils.Cfg.BrokerConnection)
//	if err != nil {
//		logger.Log.Error("Can't connect to AMQP")
//	} else {
//		logger.Log.Info("Connected to AMQP (consume)")
//	}
//	defer conn.Close()
//
//	stopChan := make(chan bool)
//
//	amqpChannel, err := conn.Channel()
//	if err != nil {
//		logger.Log.Error("Can't create a amqpChannel: ")
//	} else {
//		logger.Log.Info("amqpChannel created successful")
//	}
//	defer amqpChannel.Close()
//
//	logger.Log.Error("Creating queue ", q)
//
//	queue, err := amqpChannel.QueueDeclare(
//		q,
//		true,
//		false,
//		false,
//		false,
//		//nil,
//		amqp.Table{"x-queue-mode": "lazy"},
//	)
//	if err != nil {
//		logger.Log.Error("Could not declare queue: ", err)
//	} else {
//		logger.Log.Error("Queue declared successful")
//	}
//
//	// Create a binding between queue and exchanges to rabbitmq
//	err = amqpChannel.QueueBind(q, "", "ex-custom-actions", false, nil)
//	//err := channelRabbitMQ.QueueBind(queue, "", "award", false, nil)
//	if err != nil {
//		fmt.Println(err)
//	} else {
//		logger.Log.Warn(q, " has bind")
//	}
//
//	//err = amqpChannel.Qos(1, 0, false)
//	//handleError(err, "Could not configure QoS")
//
//	messageChannel, err := amqpChannel.Consume(
//		queue.Name,
//		"",
//		true,
//		false,
//		false,
//		false,
//		nil,
//	)
//	if err != nil {
//		logger.Log.Error("Could not register consumer: ")
//	} else {
//		logger.Log.Info("Consumer registered successful: ", queue.Name)
//	}
//
//	go func() {
//		log.Printf("Consumer ready, PID: %d", os.Getpid())
//		for d := range messageChannel {
//			log.Printf("Received a message: %s", string(d.Body))
//			//msg :=
//			//if err := d.Ack(false); err != nil {
//			//	log.Printf("Error acknowledging message : %s", err)
//			//} else {
//			//	log.Printf("Acknowledged message")
//			//}
//			_ = messageHandleWithoutQ(d.Body, ch)
//		}
//	}()
//
//	// Остановка для завершения программы
//	<-stopChan
//
//}

//func ConsumeV2(q string, ch chan []byte) {
//	logger.Log.Info("Start Consumer V2")
//
//	conn, err := amqp.Dial(utils.Cfg.BrokerConnection)
//	if err != nil {
//		logger.Log.Error("Can't connect to AMQP")
//	} else {
//		logger.Log.Info("Connected to AMQP")
//	}
//	defer conn.Close()
//
//	stopChan := make(chan bool)
//
//	amqpChannel, err := conn.Channel()
//	if err != nil {
//		logger.Log.Error("Can't create a amqpChannel: ")
//	} else {
//		logger.Log.Info("amqpChannel created successful")
//	}
//	defer amqpChannel.Close()
//
//	logger.Log.Error("Creating queue ", q)
//
//	queue, err := amqpChannel.QueueDeclare(
//		q,
//		true,
//		false,
//		false,
//		false,
//		amqp.Table{"x-queue-mode": "lazy"},
//	)
//	if err != nil {
//		logger.Log.Error("Could not declare queue: ", err)
//	} else {
//		logger.Log.Error("Queue declared successful: ")
//	}
//
//	// Create a binding between queue and exchanges to rabbitmq
//	err = amqpChannel.QueueBind(q, "", "ex-custom-actions", false, nil)
//	//err := channelRabbitMQ.QueueBind(queue, "", "award", false, nil)
//	if err != nil {
//		fmt.Println(err)
//	} else {
//		logger.Log.Warn(q, " has bind")
//	}
//
//	err = amqpChannel.Qos(1, 0, false)
//	handleError(err, "Could not configure QoS")
//
//	messageChannel, err := amqpChannel.Consume(
//		queue.Name,
//		"",
//		false,
//		false,
//		false,
//		false,
//		amqp.Table{
//			"x-queue-mode":  "lazy",
//			"x-message-ttl": 5000,
//		},
//	)
//	if err != nil {
//		logger.Log.Error("Could not register consumer: ")
//	} else {
//		logger.Log.Info("Consumer registered successful: ", queue.Name)
//	}
//
//	go func() {
//		log.Printf("Consumer ready, PID: %d", os.Getpid())
//		for d := range messageChannel {
//			log.Printf("Received a message: %v", string(d.Body))
//			msg := d.Body
//			if err := d.Ack(true); err != nil {
//				log.Printf("Error acknowledging message : %s", err)
//			} else {
//				log.Printf("Acknowledged message")
//			}
//			_ = messageHandleWithoutQ(msg, ch)
//		}
//	}()
//
//	// Остановка для завершения программы
//	<-stopChan
//
//}

// GetMessage - получение сообщения
//func GetMessage(qName string) {
//	var eName string
//	switch qName {
//	case utils.Cfg.IfnsCheckQueueName:
//		eName = utils.Cfg.IfnsCheckEventName
//		break
//	case utils.Cfg.IfnsInnQueueName:
//		eName = utils.Cfg.IfnsInnEventName
//		break
//	case utils.Cfg.SparkStatusQueueName:
//		eName = utils.Cfg.SparkStatusEventName
//		break
//	case utils.Cfg.SparkBnkQueueName:
//		eName = utils.Cfg.SparkBnkEventName
//		break
//	}
//	messageHandleWithQ := messageHandleWithoutQ(eName)
//
//	var rc RabbitConnection
//	rc.Consume(qName, messageHandleWithQ)
//
//	//logger.Log.Warn(message)
//
//}

func messageHandleWithoutQ(payload types.RequestFromRabbit, ch chan []byte) error {
	//func messageHandleWithoutQ(data types.RequestFromRabbit, ch chan []byte) error {

	//logger.Log.Warn("before marshal", string(d.Body))
	//logger.Log.Warn("after marshal", temp)
	//
	switch payload.Action.Name {
	case utils.Cfg.IfnsCheckEventName:
		go func() {
			logger.Log.Info("switch -> ifns check")
			var result types.IfnsCheckFromRestResp
			result, err := http.IfnsCheckService(payload.Id, payload.Action.Data, payload.Context)
			logger.Log.Info("Between consume and produce:", result)
			if err != nil {
				logger.Log.Fatal("Error in Rest, Produce is aborted")
			} else {
				produceIfnsCheck(payload.Id, result, ch)
			}
		}()

	case utils.Cfg.IfnsInnEventName:
		go func() {
			logger.Log.Info("switch -> ifns inn")
			result, err := http.IfnsInnService(payload.Id, payload.Action.Data, payload.Context)
			logger.Log.Info("Between result_: ", result)
			if err != nil {
				logger.Log.Fatal("Error in Rest, Produce is aborted")
			}
			ProduceIfnsInn(payload.Id, result, ch)
		}()
	case utils.Cfg.SparkStatusEventName:
		go func() {
			logger.Log.Info("switch -> spark status")
			var result types.SparkStatusFromRestResp
			result, err := http.SparkStatusService(payload.Id, payload.Action.Data, payload.Context)
			logger.Log.Info("Between result_: ", result)
			if err != nil {
				logger.Log.Fatal("Error in Rest, Produce is aborted")
			} else {
				produceSparkStatus(payload.Id, result, ch)
			}
		}()
	case utils.Cfg.SparkBnkEventName:
		go func() {
			logger.Log.Info("switch -> spark bnk")
			var result types.SparkBnkFromRestResp
			result, err := http.SparkBnkService(payload.Id, payload.Action.Data, payload.Context)
			if err != nil {
				logger.Log.Fatal("Error in Rest, Produce is aborted")
			} else {
				produceSparkBnk(payload.Id, result, ch)
			}
		}()
	}

	return nil
}

//func messageHandle(message interface{}) error {
//
//	var data types.RequestFromRabbit
//
//	logger.Log.Warn("before marshal", string(message.([]byte)))
//	//logger.Log.Warn("after marshal", temp)
//	err := json.Unmarshal(message.([]byte), &data)
//	if err != nil {
//		log.Error("Error: ", err)
//	}
//	//
//	log.Info("after marshal", data)
//
//	//
//	switch data.Action.Name {
//	case utils.Cfg.IfnsCheckEventName:
//		logger.Log.Info("switch -> ifns check")
//		var result types.IfnsCheckFromRestResp
//		result, err := http.IfnsCheckService(data.Id, data.Action.Data, data.Context)
//		logger.Log.Info("Between consume and produce:", result)
//		if err != nil {
//			logger.Log.Fatal("Error in Rest, Produce is aborted")
//		} else {
//			produceIfnsCheck(data.Id, result)
//		}
//	case utils.Cfg.IfnsInnEventName:
//		logger.Log.Info("switch -> ifns inn")
//		result, err := http.IfnsInnService(data.Id, data.Action.Data, data.Context)
//		logger.Log.Info("Between result_: ", result)
//		if err != nil {
//			logger.Log.Fatal("Error in Rest, Produce is aborted")
//		}
//		ProduceIfnsInn(data.Id, result)
//	case utils.Cfg.SparkStatusEventName:
//		logger.Log.Info("switch -> spark status")
//		var result types.SparkStatusFromRestResp
//		result, err := http.SparkStatusService(data.Id, data.Action.Data, data.Context)
//		logger.Log.Info("Between result_: ", result)
//		if err != nil {
//			logger.Log.Fatal("Error in Rest, Produce is aborted")
//		}
//		produceSparkStatus(data.Id, result)
//		if err != nil {
//			logger.Log.Fatal("Error in Rest, Produce is aborted")
//		} else {
//			produceSparkStatus(data.Id, result)
//		}
//	case utils.Cfg.SparkBnkEventName:
//		logger.Log.Info("switch -> spark bnk")
//		var result types.SparkBnkFromRestResp
//		result, err := http.SparkBnkService(data.Id, data.Action.Data, data.Context)
//		if err != nil {
//			logger.Log.Fatal("Error in Rest, Produce is aborted")
//		} else {
//			produceSparkBnk(data.Id, result)
//		}
//	}
//
//	return nil
//}
