package rabbit

import (
	"balanceplatform-api-action/logger"
	"balanceplatform-api-action/types"
	"balanceplatform-api-action/utils"
	"encoding/json"
	log "github.com/sirupsen/logrus"
)

// produceIfnsCheck -	публикация результатов сценария ifns status
func ProduceIfnsInn(eventId string, resp types.IfnsInnFromRestResp, rbTrChan chan []byte) {
	logger.Log.Warn("produceIfnsInn: ", resp)

	var rabbitResp types.IfnsInnRabbitResp
	rabbitResp.Context.Surname = resp.Surname
	rabbitResp.Context.Name = resp.Name
	rabbitResp.Context.Middlename = resp.Middlename
	rabbitResp.Context.Birthdate = resp.Birthdate
	rabbitResp.Context.Doctype = resp.Doctype
	rabbitResp.Context.Docid = resp.Docid
	rabbitResp.Context.Inn = resp.Inn
	rabbitResp.Event = utils.Cfg.IfnsInnEventName
	rabbitResp.Id = eventId
	message, err := json.Marshal(rabbitResp)
	if err != nil {
		log.Error("Error with Marshaling", err)
	} else {
		logger.Log.Warn("Marshaling is fine (rabbitMQ): ", string(message[:]))
	}

	rbTrChan <- message
	//if SendMessage(utils.Cfg.ExchangeTransmitName, string(message[:])) != nil {
	//	log.Error("Rabbit sending Error")
	//} else {
	//	logger.Log.Warn("Rabbit sending Success:", string(message[:]))
	//}

	logger.Log.Warn("whats all")
}
