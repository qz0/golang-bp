package rabbit

import (
	"balanceplatform-api-action/logger"
	"balanceplatform-api-action/utils"
	uuid "github.com/satori/go.uuid"
	"github.com/streadway/amqp"
)

func Produce(rbTrChan chan []byte, qName string) {
	logger.Log.Info("Start Producer V3")

	conn, err := amqp.Dial(utils.Cfg.BrokerConnection)
	if err != nil {
		logger.Log.Error("Can't connect to AMQP")
	} else {
		logger.Log.Info("Connected to AMQP (produce) - ", qName)
	}
	defer conn.Close()

	stopChan := make(chan bool)

	amqpChannel, err := conn.Channel()
	if err != nil {
		logger.Log.Error("Can't create a amqpChannel: ")
	} else {
		logger.Log.Info("amqpChannel created successful (produce) -", qName)
	}
	defer amqpChannel.Close()

	for msg := range rbTrChan {
		logger.Log.Info("New message: ", string(msg))

		err = amqpChannel.Publish(
			utils.Cfg.ExchangeTransmitName,
			"",
			false,
			false,
			amqp.Publishing{
				MessageId:    uuid.NewV4().String(),
				DeliveryMode: amqp.Persistent,
				Priority:     1,
				ContentType:  "plain/text",
				Body:         msg,
			})

		if err != nil {
			logger.Log.Error("Can't send message to queue")
		} else {
			logger.Log.Info("Message sent")
		}

	}

	// Остановка для завершения программы
	<-stopChan
}

// producePushMessage - публикация результатов сценария push_message
func producePushMessage(body []byte, eventId string) {
	logger.Log.Warn("producePushMessage", string(body))
	//var rabbitResp types.AwardRabbitResp
	//rabbitResp.Context.OfferId = fmt.Sprintf("%d", offerId)
	//rabbitResp.Event = utils.Cfg.AwardEventName
	//rabbitResp.Id = eventId
	//message, err := json.Marshal(rabbitResp)
	//if err != nil {
	//	log.Error("Error with Marshaling", err)
	//} else {
	//	logger.Log.Warn("Marshaling is fine")
	//}
	//
	//if SendMessage(utils.Cfg.ExchangeName, string(message[:])) != nil {
	//	log.Error("Rabbit sending Error")
	//} else {
	//	logger.Log.Warn("Rabbit sending Success")
	//}
	//
	//logger.Log.Warn("whats all")
}

// producePushOffer - публикация результатов сценария push_offer
func producePushOffer(body []byte, eventId string) {
	logger.Log.Warn("producePushOffer", string(body))
	//var rabbitResp types.AwardRabbitResp
	//rabbitResp.Context.OfferId = fmt.Sprintf("%d", offerId)
	//rabbitResp.Event = utils.Cfg.AwardEventName
	//rabbitResp.Id = eventId
	//message, err := json.Marshal(rabbitResp)
	//if err != nil {
	//	log.Error("Error with Marshaling", err)
	//} else {
	//	logger.Log.Warn("Marshaling is fine")
	//}
	//
	//if SendMessage(utils.Cfg.ExchangeName, string(message[:])) != nil {
	//	log.Error("Rabbit sending Error")
	//} else {
	//	logger.Log.Warn("Rabbit sending Success")
	//}
	//
	//logger.Log.Warn("whats all")
}

// producePushAward - публикация результатов сценария push_award
func producePushAward(body []byte, eventId string) {
	logger.Log.Warn("producePushAward", string(body))
	//var rabbitResp types.AwardRabbitResp
	//rabbitResp.Context.OfferId = fmt.Sprintf("%d", offerId)
	//rabbitResp.Event = utils.Cfg.AwardEventName
	//rabbitResp.Id = eventId
	//message, err := json.Marshal(rabbitResp)
	//if err != nil {
	//	log.Error("Error with Marshaling", err)
	//} else {
	//	logger.Log.Warn("Marshaling is fine")
	//}
	//
	//if SendMessage(utils.Cfg.ExchangeName, string(message[:])) != nil {
	//	log.Error("Rabbit sending Error")
	//} else {
	//	logger.Log.Warn("Rabbit sending Success")
	//}
	//
	//logger.Log.Warn("whats all")
}
