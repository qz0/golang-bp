package rabbit

import (
	"balanceplatform-api-action/logger"
	"balanceplatform-api-action/types"
	"balanceplatform-api-action/utils"
	"encoding/json"
	log "github.com/sirupsen/logrus"
)

// produceSparkBnk -	публикация результатов сценария Spark bnk
func produceSparkBnk(eventId string, resp types.SparkBnkFromRestResp, rbTrChan chan []byte) {
	var rabbitResp types.SparkBnkRabbitResp
	rabbitResp.Context.Inn = resp.Inn
	rabbitResp.Context.Result = resp.Result
	rabbitResp.Event = utils.Cfg.SparkBnkEventName
	rabbitResp.Id = eventId
	message, err := json.Marshal(rabbitResp)
	if err != nil {
		log.Error("Error with Marshaling", err)
	} else {
		logger.Log.Warn("Marshaling is fine: ", string(message[:]))
	}

	rbTrChan <- message
	//if SendMessage(utils.Cfg.ExchangeTransmitName, string(message[:])) != nil {
	//	log.Error("Rabbit sending Error")
	//} else {
	//	logger.Log.Warn("Rabbit sending Success:", string(message[:]))
	//}

	logger.Log.Warn("whats all")
}
