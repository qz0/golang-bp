package rabbit

// QueuesRegistration - binding queues in exchanges
//func QueuesRegistration() {
//	logger.Log.Warn("start queue registration function")
//	// queues
//	var queues = []string{utils.Cfg.IfnsCheckQueueName}
//
//	//logger.Log.Warn("Try to read ifns event", utils.Cfg.IfnsEventName)
//
//	// Create a new RabbitMQ connection.
//	//connectRabbitMQ, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/", utils.Cfg.RabbitUsername, utils.Cfg.RabbitPassword, utils.Cfg.RabbitHost, utils.Cfg.RabbitPort))
//	connectRabbitMQ, err := amqp.Dial(utils.Cfg.BrokerConnection)
//	if err != nil {
//		panic(err)
//	} else {
//		logger.Log.Warn("create connect to rabbitMQ", utils.Cfg.BrokerConnection)
//	}
//	defer connectRabbitMQ.Close()
//
//	// Let's start by opening a channel to our RabbitMQ
//	// instance over the connection we have already
//	// established.
//	channelRabbitMQ, err := connectRabbitMQ.Channel()
//	if err != nil {
//		panic(err)
//	}
//	defer channelRabbitMQ.Close()
//
//	for _, queue := range queues {
//		logger.Log.Warn("declaring =>", queue)
//		// Create a binding between queue and exchanges to rabbitmq
//		q, err := channelRabbitMQ.QueueDeclare(queue, true, false, false, false, amqp.Table{"x-queue-mode": "lazy"})
//		logger.Log.Warn("declared =>", q.Name)
//		//err := channelRabbitMQ.QueueBind(queue, "", "award", false, nil)
//		if err != nil {
//			logger.Log.Warn("declaration error", q.Name, err)
//		}
//		// Create a binding between queue and exchanges to rabbitmq
//		err = channelRabbitMQ.QueueBind(queue, "", "ex-custom-actions", false, nil)
//		//err := channelRabbitMQ.QueueBind(queue, "", "award", false, nil)
//		if err != nil {
//			fmt.Println(err)
//		} else {
//			logger.Log.Warn("has bind =>", q.Name)
//		}
//	}
//
//	logger.Log.Warn("finiosh queue registration function")
//}
