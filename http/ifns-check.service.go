package http

import (
	"balanceplatform-api-action/logger"
	"balanceplatform-api-action/types"
	"balanceplatform-api-action/utils"
	"encoding/json"
	"github.com/go-resty/resty/v2"
	log "github.com/sirupsen/logrus"
	_ "strconv"
)

// IfnsCheckService - сервис для отправки REST для ifns check
func IfnsCheckService(id string, _data interface{}, ctx map[string]interface{}) (types.IfnsCheckFromRestResp, error) {

	var data = _data.(map[string]interface{})
	logger.Log.Info("IfnsCheckService")

	var err error

	//	INN
	var innCtx string
	temp, ok := ctx["inn"]
	if ok {
		innCtx = temp.(string)
	}

	var innData string
	temp, ok = data["inn"]
	if ok {
		innData = temp.(string)
	}

	var inn string
	if len(innData) != 0 {
		inn = innData
	} else {
		inn = innCtx
	}

	//	http request
	var restReq types.IfnsCheckToRestReq
	var respFromRest types.IfnsCheckFromRestResp

	restReq.Inn = inn

	body, err := json.Marshal(restReq)
	if err != nil {
		log.Error("Error with Marshaling", err)
		return types.IfnsCheckFromRestResp{}, err
	} else {
		logger.Log.Warn("Marshaling is fine")
	}

	log.Info("URL: ", utils.Cfg.IfnsCheckURL)

	client := resty.New()
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(string(body)).
		SetResult(&respFromRest). // or SetResult(AuthSuccess{}).
		Post(utils.Cfg.IfnsCheckURL)

	if err != nil {
		log.Error("error throw request")
		return types.IfnsCheckFromRestResp{}, err
	} else {
		log.Info("Status code: ", resp.StatusCode())
		log.Info("Body: ", string(resp.Body()))
		log.Info("respFromRest: ", respFromRest)
		return respFromRest, nil
	}

}
