package http

import (
	"balanceplatform-api-action/logger"
	"balanceplatform-api-action/types"
	"balanceplatform-api-action/utils"
	"encoding/json"
	"github.com/go-resty/resty/v2"
	log "github.com/sirupsen/logrus"
)

func TryTestInn() (types.IfnsInnFromRestResp, error) {

	var restReq types.IfnsInnToRestReq
	var respFromRest types.IfnsInnFromRestResp

	restReq.Name = "Alex"
	restReq.Surname = "Petrov"
	restReq.Middlename = "Ivan"
	restReq.Birthdate = "22-04-1988"
	restReq.Doctype = "passport"
	restReq.Docid = "4408234235"
	//restReq.Inn = "4408234235"

	//tr := &http.Transport{
	//	TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	//}
	//	объявляем клиента для запроса
	//client := &http.Client{Transport: tr}

	body, err := json.Marshal(restReq)
	if err != nil {
		log.Error("Error with Marshaling", err)
	} else {
		logger.Log.Warn("Marshaling is fine")

		logger.Log.Warn("restReq: ", string(body))
		//logger.Log.Warn("restReq: ", "{\"surname\":\"Petrov\",\"name\":\"Alex\",\"middlename\":\"Ivan\",\"birthdate\":\"22-04-1988\",\"doctype\":\"passport\",\"docid\":\"4408234235\",}")
		logger.Log.Warn("IfnsInnURL: ", utils.Cfg.IfnsInnURL)

	}

	client := resty.New()
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(string(body)).
		SetResult(&respFromRest). // or SetResult(AuthSuccess{}).
		Post(utils.Cfg.IfnsInnURL)

	if err != nil {
		log.Error("error throw request")
	} else {
		log.Info("Status code: ", resp.StatusCode())
		log.Info("Body: ", string(resp.Body()))
		log.Info("respFromRest: ", respFromRest)
	}

	logger.Log.Warn("End of REST Service: ")

	return respFromRest, nil
}
