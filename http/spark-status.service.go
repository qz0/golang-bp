package http

import (
	"balanceplatform-api-action/logger"
	"balanceplatform-api-action/types"
	"balanceplatform-api-action/utils"
	"encoding/json"
	"github.com/go-resty/resty/v2"
	log "github.com/sirupsen/logrus"
	_ "strconv"
)

// SparkStatusService - сервис для отправки REST для spark status
func SparkStatusService(id string, _data interface{}, ctx map[string]interface{}) (types.SparkStatusFromRestResp, error) {

	var data = _data.(map[string]interface{})
	logger.Log.Info("SparkStatusService")

	var err error

	//	INN
	var innCtx string
	temp, ok := ctx["inn"]
	if ok {
		innCtx = temp.(string)
	}

	var innData string
	temp, ok = data["inn"]
	if ok {
		innData = temp.(string)
	}

	var inn string
	if len(innData) != 0 {
		inn = innData
	} else {
		inn = innCtx
	}

	//	http request
	var restReq types.SparkStatusToRestReq
	var respFromRest types.SparkStatusFromRestResp

	restReq.Inn = inn

	body, err := json.Marshal(restReq)
	if err != nil {
		log.Error("Error with Marshaling", err)
		return types.SparkStatusFromRestResp{}, err
	} else {
		logger.Log.Warn("Marshaling is fine")
	}

	client := resty.New()
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(string(body)).
		SetResult(&respFromRest). // or SetResult(AuthSuccess{}).
		Post(utils.Cfg.SparkStatusURL)

	if err != nil {
		log.Error("error throw request")
		return types.SparkStatusFromRestResp{}, err
	} else {
		log.Info("Status code: ", resp.StatusCode())
		log.Info("Body: ", string(resp.Body()))
		log.Info("respFromRest: ", respFromRest)
		logger.Log.Warn("End of REST Service: ")

		return respFromRest, nil
	}

}
