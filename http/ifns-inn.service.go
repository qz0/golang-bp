package http

import (
	"balanceplatform-api-action/logger"
	"balanceplatform-api-action/types"
	"balanceplatform-api-action/utils"
	"encoding/json"
	"github.com/go-resty/resty/v2"
	log "github.com/sirupsen/logrus"
	_ "strconv"
)

// IfnsInnService - сервис для отправки REST для ifns inn
func IfnsInnService(id string, _data interface{}, ctx map[string]interface{}) (types.IfnsInnFromRestResp, error) {

	var data = _data.(map[string]interface{})
	logger.Log.Info("IfnsInnService")

	var err error

	//	Values from ctx
	var nameCtx string
	temp, ok := ctx["name"]
	if ok {
		nameCtx = temp.(string)
	}

	var surnameCtx string
	temp, ok = ctx["surname"]
	if ok {
		surnameCtx = temp.(string)
	}

	var middlenameCtx string
	temp, ok = ctx["middlename"]
	if ok {
		middlenameCtx = temp.(string)
	}

	var birthdateCtx string
	temp, ok = ctx["birthdate"]
	if ok {
		birthdateCtx = temp.(string)
	}

	var doctypeCtx string
	temp, ok = ctx["doctype"]
	if ok {
		doctypeCtx = temp.(string)
	}

	var docidCtx string
	temp, ok = ctx["docid"]
	if ok {
		docidCtx = temp.(string)
	}

	//	Values from Data
	var nameData string
	temp, ok = data["name"]
	if ok {
		nameData = temp.(string)
	}

	var surnameData string
	temp, ok = data["surname"]
	if ok {
		surnameData = temp.(string)
	}

	var middlenameData string
	temp, ok = data["middlename"]
	if ok {
		middlenameData = temp.(string)
	}

	var birthdateData string
	temp, ok = data["birthdate"]
	if ok {
		birthdateData = temp.(string)
	}

	var doctypeData string
	temp, ok = data["doctype"]
	if ok {
		doctypeData = temp.(string)
	}

	var docidData string
	temp, ok = data["docid"]
	if ok {
		docidData = temp.(string)
	}

	//	Values for sending
	var name string
	if len(nameData) != 0 {
		name = nameData
	} else {
		name = nameCtx
	}
	logger.Log.Warn("name: ", name)

	var surname string
	if len(surnameData) != 0 {
		surname = surnameData
	} else {
		surname = surnameCtx
	}
	logger.Log.Warn("surname: ", surname)

	var middlename string
	if len(middlenameData) != 0 {
		middlename = middlenameData
	} else {
		middlename = middlenameCtx
	}
	logger.Log.Warn("middlename: ", middlename)

	var birthdate string
	if len(birthdateData) != 0 {
		birthdate = birthdateData
	} else {
		birthdate = birthdateCtx
	}
	logger.Log.Warn("birthdate: ", birthdate)

	var doctype string
	if len(doctypeData) != 0 {
		doctype = doctypeData
	} else {
		doctype = doctypeCtx
	}
	logger.Log.Warn("doctype: ", doctype)

	var docid string
	if len(docidData) != 0 {
		docid = docidData
	} else {
		docid = docidCtx
	}
	logger.Log.Warn("docid: ", docid)

	//	http request
	var restReq types.IfnsInnToRestReq
	var respFromRest types.IfnsInnFromRestResp

	restReq.Name = name
	restReq.Surname = surname
	restReq.Middlename = middlename
	restReq.Birthdate = birthdate
	restReq.Doctype = doctype
	restReq.Docid = docid

	body, err := json.Marshal(restReq)
	if err != nil {
		log.Error("Error with Marshaling", err)
		return types.IfnsInnFromRestResp{}, err
	} else {
		logger.Log.Warn("Marshaling is fine")
		logger.Log.Warn("restReq: ", string(body))
		logger.Log.Warn("IfnsInnURL: ", utils.Cfg.IfnsInnURL)

	}

	client := resty.New()
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(string(body)).
		SetResult(&respFromRest). // or SetResult(AuthSuccess{}).
		Post(utils.Cfg.IfnsInnURL)

	if err != nil {
		log.Error("error throw request")
		return types.IfnsInnFromRestResp{}, err
	} else {
		log.Info("Status code: ", resp.StatusCode())
		log.Info("Body: ", string(resp.Body()))
		log.Info("respFromRest: ", respFromRest)
		logger.Log.Warn("End of REST Service: ")

		return respFromRest, nil
	}

}
