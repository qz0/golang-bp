package http

import (
	"encoding/json"
	"net/http"
)

// statusOK - статус ОК
func statusOK(w http.ResponseWriter, r *http.Request) {
	// Here we adding headers
	// w.Header().Set("Access-Control-Allow-Headers", "*")
	// w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH, OPTIONS")
	// // http.SetCookie(w, &http.Cookie{Name: "api_key", Value: app.GetAPIKey()})
	// w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	// inner.ServeHTTP(w, r)
	w.Header().Set("Access-Control-Allow-Origin", "*")
	json.NewEncoder(w).Encode("{'status':'OK'}")
	return
}
