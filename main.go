package main

import (
	"balanceplatform-api-action/logger"
	"balanceplatform-api-action/rabbit"
	"balanceplatform-api-action/redis"
	"balanceplatform-api-action/utils"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
)

const VERSION string = "0.0.45"

func main() {
	logger.Log.Warn("Version: ", VERSION)
	rbTrChan1 := make(chan []byte)
	rbTrChan2 := make(chan []byte)
	rbTrChan3 := make(chan []byte)
	rbTrChan4 := make(chan []byte)

	var err error

	utils.Cfg, err = utils.LoadConfig(".")
	if err != nil {
		log.Fatal("cannot load config", err)
	} else {
		logger.Log.Warn("Load config is successful", utils.Cfg)
	}

	// выставляю уровень логов. Раскоментить желаемое что бы выбрать уровень с которого отображаются логи..
	if utils.Cfg.LogLevel == "DebugLevel" {
		logger.Log.SetLevel(log.DebugLevel)
		logger.Log.Warn("DebugLevel")
	} else {
		logger.Log.SetLevel(log.InfoLevel)
		logger.Log.Out = ioutil.Discard
	}

	logger.Log.Warn("Utils config before action registration: ", utils.Cfg)

	//rabbit.TestSpecial()

	redis.ActionRegistration()

	//rabbit.QueuesRegistration()

	go rabbit.Produce(rbTrChan1, utils.Cfg.IfnsCheckQueue)
	go rabbit.Produce(rbTrChan2, utils.Cfg.IfnsInnQueue)
	go rabbit.Produce(rbTrChan3, utils.Cfg.SparkStatusQueue)
	go rabbit.Produce(rbTrChan4, utils.Cfg.SparkBnkQueue)
	//rabbit.ConsumeV2(utils.Cfg.QueueName, rbTrChan)
	//rabbit.ConsumeV3(utils.Cfg.QueueName, rbTrChan)
	go rabbit.ConsumeV4(utils.Cfg.IfnsCheckQueue, rbTrChan1)
	go rabbit.ConsumeV4(utils.Cfg.IfnsInnQueue, rbTrChan2)
	go rabbit.ConsumeV4(utils.Cfg.SparkStatusQueue, rbTrChan3)
	rabbit.ConsumeV4(utils.Cfg.SparkBnkQueue, rbTrChan4)
	//rabbit.ConsumeV2(utils.Cfg.IfnsInnQueueName)
	//go rabbit.GetMessage(utils.Cfg.IfnsCheckQueueName)
	//go rabbit.GetMessage(utils.Cfg.IfnsInnQueueName)
	//go rabbit.GetMessage(utils.Cfg.SparkBnkQueueName)
	//rabbit.GetMessage(utils.Cfg.SparkStatusQueueName)

	// Включаем роутер
	//http.CreateWebServer(9000, true )

}
