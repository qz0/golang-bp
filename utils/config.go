package utils

import (
	"github.com/spf13/viper"
)

type Config struct {
	LogLevel             string `mapstructure:"LOG_LEVEL"`
	RedisConnection      string `mapstructure:"REDIS_CONNECTION"`
	RedisKeyName         string `mapstructure:"REDIS_KEY_NAME"`
	BrokerConnection     string `mapstructure:"BROKER_CONNECTION"`
	ExchangeTransmitName string `mapstructure:"EXCHANGE_TRANSMIT_NAME"`
	ExchangeReceiveName  string `mapstructure:"EXCHANGE_RECEIVE_NAME"`
	SparkStatusEventName string `mapstructure:"SPARK_STATUS_EVENT_NAME"`
	SparkStatusURL       string `mapstructure:"SPARK_STATUS_URL"`
	SparkStatusQueue     string `mapstructure:"SPARK_STATUS_QUEUE"`
	SparkBnkEventName    string `mapstructure:"SPARK_BNK_EVENT_NAME"`
	SparkBnkURL          string `mapstructure:"SPARK_BNK_URL"`
	SparkBnkQueue        string `mapstructure:"SPARK_BNK_QUEUE"`
	QueueName            string `mapstructure:"QUEUE_NAME"`
	IfnsCheckEventName   string `mapstructure:"IFNS_CHECK_EVENT_NAME"`
	IfnsCheckURL         string `mapstructure:"IFNS_CHECK_URL"`
	IfnsCheckQueue       string `mapstructure:"IFNS_CHECK_QUEUE"`
	IfnsInnEventName     string `mapstructure:"IFNS_INN_EVENT_NAME"`
	IfnsInnURL           string `mapstructure:"IFNS_INN_URL"`
	IfnsInnQueue         string `mapstructure:"IFNS_INN_QUEUE"`
}

var Cfg Config

//var RMQ types.RabbitConf

// LoadConfig reads configuration from file or environment variables.
func LoadConfig(path string) (config Config, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName("app")
	viper.SetConfigType("env")

	viper.AutomaticEnv()
	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)
	return
}
