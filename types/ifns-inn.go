package types

// Ifns inn
type IfnsInnFromRestResp struct {
	Surname    string `json:"surname"`
	Name       string `json:"name"`
	Middlename string `json:"middlename"`
	Birthdate  string `json:"birthdate"`
	Doctype    string `json:"doctype"`
	Docid      string `json:"docid"`
	Inn        string `json:"inn"`
}

type IfnsInnToRestReq struct {
	Name       string `json:"name"`
	Surname    string `json:"surname"`
	Middlename string `json:"middlename"`
	Birthdate  string `json:"birthdate"`
	Doctype    string `json:"doctype"`
	Docid      string `json:"docid"`
}

type IfnsInnRabbitResp struct {
	Id      string              `json:"id"`
	Event   string              `json:"event"`
	Context IfnsInnFromRestResp `json:"context"`
}
