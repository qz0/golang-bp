package types

type RabbitConf struct {
	Username string
	Password string
	Host     string
	Port     string
}

type RequestFromRabbit struct {
	Id     string `json:"id"`
	Event  string `json:"event"`
	Action struct {
		Id   int         `json:"id"`
		Name string      `json:"name"`
		Data interface{} `json:"data"`
	} `json:"action"`
	Context  map[string]interface{} `json:"context"`
	FlowName string                 `json:"flow_name"`
	FlowId   string                 `json:"flow_id"`
}
