package types

// SparkBnk

type SparkBnkToRestReq struct {
	Inn string `json:"inn"`
}

type SparkBnkFromRestResp struct {
	Inn    string `json:"inn"`
	Result bool   `json:"result"`
}

type SparkBnkRabbitResp struct {
	Id      string               `json:"id"`
	Event   string               `json:"event"`
	Context SparkBnkFromRestResp `json:"context"`
}
