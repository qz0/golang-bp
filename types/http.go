package types

import "net/http"

// структура для маршрута
type Route struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Method      string `json:"method"`
	// APIVersion  int              `json:"api_version"`
	Path        string           `json:"pattern"`
	HandlerFunc http.HandlerFunc `json:"-"`
}
