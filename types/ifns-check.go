package types

type IfnsCheckToRestReq struct {
	Inn string `json:"inn"`
}

type IfnsCheckRabbitResp struct {
	Id      string                `json:"id"`
	Event   string                `json:"event"`
	Context IfnsCheckFromRestResp `json:"context"`
}

type IfnsCheckFromRestResp struct {
	Inn    string `json:"inn"`
	Result bool   `json:"result"`
}
