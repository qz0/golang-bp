package types

// REQUEST to REST
//

// Ifns inn

// SPARK
type SparkStatusToRestReq struct {
	Inn string `json:"inn"`
}

// RESPONSE from REST
//
//	Ifns check

// SparkStatus
type SparkStatusFromRestResp struct {
	Inn    string `json:"inn"`
	Result string `json:"result"`
}

type SparkStatusRabbitResp struct {
	Id      string                  `json:"id"`
	Event   string                  `json:"event"`
	Context SparkStatusFromRestResp `json:"context"`
}

// SPECIAL
//type AdditionalInfoType struct {
//	Partner string `json:"partner"`
//}
